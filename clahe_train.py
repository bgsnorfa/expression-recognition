import cv2
import numpy as np
import glob
import random
from keras.utils import np_utils
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.optimizers import SGD,RMSprop
from keras.constraints import maxnorm

# emotions = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"]
emotions = ["neutral", "anger", "disgust", "happy", "surprise"]
# emotions = ["neutral", "anger", "happy"]

clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))

def get_files(emotion): #Define function to get file list, randomly shuffle it and split 80/20
	files = glob.glob("dlib_dataset/%s/*" %emotion)
	random.shuffle(files)
	training = files[:int(len(files)*0.8)] #get first 80% of file list
	prediction = files[-int(len(files)*0.2):] #get last 20% of file list

	return training, prediction

def make_sets():
	training_data = []
	training_labels = []
	prediction_data = []
	prediction_labels = []
	for emotion in emotions:
		training, prediction = get_files(emotion)
		for item in training:
			image = cv2.imread(item) #open image
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY) #convert to grayscale
			resized = cv2.resize(gray, (32, 32))
			resized = clahe.apply(resized)
			training_data.append(resized) #append image array to training data list
			training_labels.append(emotions.index(emotion))

		for item in prediction: #repeat above process for prediction set
			image = cv2.imread(item)
			gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)
			resized = cv2.resize(gray, (32, 32))
			resized = clahe.apply(resized)
			prediction_data.append(resized)
			prediction_labels.append(emotions.index(emotion))

	return training_data, training_labels, prediction_data, prediction_labels

def refine_data(data, label, classes):
	x = np.array(data)
	x = x.astype('float32')
	x /= 255
	x = np.expand_dims(x, axis=4)

	y = np.array(label, dtype="int64")
	y = np_utils.to_categorical(y, classes)

	return x, y

X_train, y_train, X_test, y_test = make_sets()
num_classes = len(emotions)
print("%d labels in dataset" %num_classes)
print("training data is : ", len(y_train), "images")
print("testing data is : ", len(y_test), "images")

# refine dataset for cnn
X_train, y_train = refine_data(X_train, y_train, num_classes)
X_test, y_test = refine_data(X_test, y_test, num_classes)

input_shape = X_test[0].shape

model = Sequential()

model.add(Conv2D(32, (5, 5), input_shape=input_shape, padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Conv2D(32, (5, 5), padding='same', activation='relu'))
model.add(MaxPooling2D(pool_size=(2, 2)))

model.add(Flatten())
model.add(Dense(25, activation='relu'))
model.add(Dropout(0.5))
model.add(Dense(num_classes, activation='softmax'))
model.compile(loss="categorical_crossentropy", optimizer="rmsprop", metrics=['accuracy'])
model.fit(X_train, y_train, batch_size=5, epochs=50)

model.save('clahe_cnn_model.h5')

# model = load_model('clahe_cnn_model.h5')

score = model.evaluate(X_test, y_test, verbose=1)
print("test loss: ", score[0])
print("test accuracy: ", score[1] * 100)