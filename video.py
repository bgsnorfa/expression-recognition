import cv2
import numpy as np
import dlib
import imutils
from imutils import face_utils
from keras.models import load_model

# expression = ["neutral", "anger", "contempt", "disgust", "fear", "happy", "sadness", "surprise"]
expression = ["neutral", "anger", "disgust", "happy", "surprise"]
face_classifier = cv2.CascadeClassifier('haarcascade_frontalface_default.xml')
model = load_model('normalize_cnn_model.h5')

def face_detector(img, size=0.5):
	gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
	faces = face_classifier.detectMultiScale(gray, 1.3, 5)
	if faces is ():
		return img

	for (x, y, w, h) in faces:
		
		cv2.rectangle(img, (x, y), (x+w, y+h), (255, 0, 0), 2)
		face = gray[y:y+h, x:x+w]
		face = cv2.resize(face, (32, 32))

		face = np.array(face)
		face = face.astype('float32')
		face /= 255
		face = np.expand_dims(face, axis=4)
		face = face.reshape(1, 32, 32, 1)
		answer = model.predict(face)
		result = expression[np.argmax(answer)]
		cv2.putText(img, result, (x, y), cv2.FONT_HERSHEY_SIMPLEX, 1, (0, 255, 0), 2)
		print(result)
		
	return img

cap = cv2.VideoCapture(0)

while True:
	ret, frame = cap.read()
	cv2.imshow('Face extractor', face_detector(frame))
	if cv2.waitKey(1) == 13:
		break